set(neurofires_repo_type git)
set(neurofires_repo_url "git@gitlab.gmrv.es:retrieval/neurofires.git")
set(neurofires_repo_tag "master")
set(neurofires_depends fires nsol)
